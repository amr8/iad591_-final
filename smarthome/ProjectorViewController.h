//
//  ProjectorViewController.h
//  smarthome
//
//  Created by Andy Nguyen on 6/28/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "FanControlTableViewCell.h"
#import <BFRadialWaveHUD/BFRadialWaveHUD.h>
#import "CTDataCenter.h"
#import "SCLAlertView.h"
#import <AFNetworking/AFNetworking.h>

@interface ProjectorViewController : UIViewController

@end
