//
//  RoomCollectionViewCell.h
//  smarthome
//
//  Created by Andy Nguyen on 6/26/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblRoomName;
@property (weak, nonatomic) IBOutlet UILabel *lblDeviceEnable;

@end
