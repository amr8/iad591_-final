//
//  ViewController.m
//  smarthome
//
//  Created by Andy Nguyen on 6/25/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view, typically from a nib.
    self.dataRoom = [CTDB._dataRoom mutableCopy];
    CTDB._roomIndex = 0;
    if (self.dataRoom==nil) {
        self.dataRoom = [[NSMutableArray alloc] initWithObjects:@"Living Room",@"Workspace",@"Bedroom 1",@"Bedroom 2", nil];
        CTDB._dataRoom = self.dataRoom;
    }
    
    NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];
    self.dataDevice = [temp mutableCopy];
    NSLog(@"%@ -- %ld",CTDB._dataRoom,CTDB._roomIndex);
    [self.collectionViewDevices reloadData];
    
    if (self.dataDevice==nil) {
        self.dataDevice = [[NSMutableArray alloc] initWithObjects:@"Lights",@"Cameras",@"Climate",@"Safety",@"Television",nil];
        [[NSUserDefaults standardUserDefaults] setValue:self.dataDevice forKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];
    }
    [ProgressHUD spinnerColor:[UIColor colorWithRed:65/255.0f green:117/255.0f blue:5/255.0f alpha:1.0f]];
    [ProgressHUD statusColor:[UIColor colorWithRed:65/255.0f green:117/255.0f blue:5/255.0f alpha:1.0f]];
    [ProgressHUD backgroundColor:[UIColor colorWithRed:74/255.0f green:74/255.0f blue:74/255.0f alpha:1.0f]];
    [ProgressHUD show:@"Please wait..."];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/api/socket/listDevices",HOST_URL] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [ProgressHUD dismiss];
        DeviceCollectionViewCell *cell = (DeviceCollectionViewCell *)[self.collectionViewDevices cellForItemAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:0]];
        if (responseObject==nil || [responseObject count]==0) {
            return ;
        } else {
            NSArray *temp = [[[responseObject objectAtIndex:0] valueForKey:@"temprature"] componentsSeparatedByString:@" "];
            NSArray *humi = [[[responseObject objectAtIndex:0] valueForKey:@"humidity"] componentsSeparatedByString:@"%"];
            
            self.temperature = [temp objectAtIndex:0];
            self.humidity = [humi objectAtIndex:0];
            
            [cell.lblValue setText:self.temperature];
            [self.collectionViewDevices reloadData];
            NSLog(@"JSON: %@", responseObject);

        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [ProgressHUD dismiss];
        NSLog(@"Error: %@", error);
    }];
    
    NSURL* url = [[NSURL alloc] initWithString:HOST_URL];
    self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
    SocketIOClient* socket = self.manager.defaultSocket;
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
    }];
    
    [socket on:@"7e:e9:17:2d:e6:b4_re-temp" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSString *cur = [data objectAtIndex:0];
        NSLog(@"Temprature : %@",cur);
        DeviceCollectionViewCell *cell = (DeviceCollectionViewCell *)[self.collectionViewDevices cellForItemAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:0]];
        NSArray *listClimate = [cur componentsSeparatedByString:@"|"];
        
        self.temperature = [[[listClimate objectAtIndex:0] componentsSeparatedByString:@" "] objectAtIndex:0];
        self.humidity = [[[listClimate objectAtIndex:1] componentsSeparatedByString:@"%"] objectAtIndex:0];
        NSArray *array = [NSArray arrayWithObjects:self.temperature,self.humidity, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_CLIMATE_UI" object:[array mutableCopy]];

        [cell.lblValue setText:self.temperature];
        [self.collectionViewDevices reloadData];
    }];
    
    [socket on:@"7e:e9:17:2d:e6:b4_change_stage" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSString *cur = [data objectAtIndex:0];
        NSLog(@"State : %@",cur);
        if ([cur isEqualToString:@"ON"]) {
            [self.imgEntranceDoor setHighlighted:YES];
        } else {
            [self.imgEntranceDoor setHighlighted:NO];
        }
    }];
    
    [socket connect];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDeviceListUI) name:@"UPDATE_DEVICE_LIST_UI" object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)updateDeviceListUI {
    self.dataDevice = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];
    [self.collectionViewDevices reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Collection view delegate and datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([collectionView isEqual:self.collectionViewTabs]) {
        return [self.dataRoom count];
    } else {
        return [self.dataDevice count];
    }
    
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:self.collectionViewTabs]) {
        RoomCollectionViewCell *cell;
        if (indexPath.row==0) {
            cell = (RoomCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"LivingRoomCellIndentifier" forIndexPath:indexPath];
            NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:indexPath.row],indexPath.row]];
            cell.lblValue.text = [NSString stringWithFormat:@"%ld",[temp count]];

        } else {
            cell = (RoomCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"RoomCellIndentifier" forIndexPath:indexPath];
            cell.lblRoomName.text = [self.dataRoom objectAtIndex:indexPath.row];
            NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:indexPath.row],indexPath.row]];
            cell.lblValue.text = [NSString stringWithFormat:@"%ld",[temp count]];
        }
        return cell;
    } else {
        UICollectionViewCell *cell;
        NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];

        if ([temp count]>0) {
            if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Lights"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LightsCellIndentifier" forIndexPath:indexPath];
            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Projector"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProjectorCellIndentifier" forIndexPath:indexPath];

            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Cameras"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CamerasCellIndentifier" forIndexPath:indexPath];

            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Climate"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ClimateCellIndentifier" forIndexPath:indexPath];

            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Safety"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SafetyCellIndentifier" forIndexPath:indexPath];

            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Television"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TelevisionCellIndentifier" forIndexPath:indexPath];

            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Fan"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FanCellIndentifier" forIndexPath:indexPath];
            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Air Conditioner"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AirConditionerCellIndentifier" forIndexPath:indexPath];
            } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Sound"]) {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SoundCellIndentifier" forIndexPath:indexPath];
            }
        }
        
        if (indexPath.row%2==0) {
            CGRect frame = cell.contentView.frame;
            frame = CGRectMake(5, frame.origin.y, frame.size.width, frame.size.height);
            cell.contentView.frame = frame;
        } else {
            CGRect frame = cell.contentView.frame;
            frame = CGRectMake(-5, frame.origin.y, frame.size.width, frame.size.height);
            cell.contentView.frame = frame;
        }
        return cell;

    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==self.collectionViewTabs) {
        CTDB._roomIndex = indexPath.row;
        NSLog(@" Room key %@",[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]);
//        self.dataDevice = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];
//        [self.collectionViewDevices reloadData];
        [self updateDeviceListUI];
    } else {
        NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[self.dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];
        if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Fan"]) {
            FanControlViewController *fan = [[FanControlViewController alloc] initWithNibName:@"FanControlViewController" bundle:nil];
            fan.strTitle = @"Fan Control";
            fan.deviceName = [temp objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:fan animated:YES];
        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Climate"]) {
            ClimateViewController *climate = [[ClimateViewController alloc] initWithNibName:@"ClimateViewController" bundle:nil];
            climate.humidity = self.humidity;
            climate.temperature = self.temperature;
            [self.navigationController pushViewController:climate animated:YES];

        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Safety"]) {
            SafetyViewController *safety = [[SafetyViewController alloc] initWithNibName:@"SafetyViewController" bundle:nil];
            [self.navigationController pushViewController:safety animated:YES];
            
        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Air Conditioner"]) {
            FanControlViewController *air = [[FanControlViewController alloc] initWithNibName:@"FanControlViewController" bundle:nil];
            air.strTitle = @"Air Conditioner Control";
            air.deviceName = [temp objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:air animated:YES];

        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Cameras"]) {
            CamerasViewController *camera = [[CamerasViewController alloc] initWithNibName:@"CamerasViewController" bundle:nil];
            [self.navigationController pushViewController:camera animated:YES];

        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Television"]) {
            FanControlViewController *tv = [[FanControlViewController alloc] initWithNibName:@"FanControlViewController" bundle:nil];
            tv.strTitle = @"Television Control";
            tv.deviceName = [temp objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:tv animated:YES];

        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Lights"]) {
            LightsViewController *lights = [[LightsViewController alloc] initWithNibName:@"LightsViewController" bundle:nil];
            [self.navigationController pushViewController:lights animated:YES];

        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Projector"]) {
            FanControlViewController *projector = [[FanControlViewController alloc] initWithNibName:@"FanControlViewController" bundle:nil];
            projector.strTitle = @"Projector Control";
            projector.deviceName = [temp objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:projector animated:YES];
        } else if ([[temp objectAtIndex:indexPath.row] isEqualToString:@"Sound"]) {
            FanControlViewController *sound = [[FanControlViewController alloc] initWithNibName:@"FanControlViewController" bundle:nil];
            sound.strTitle = @"Sound Control";
            sound.deviceName = [temp objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:sound animated:YES];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize size;
    if ([collectionView isEqual:self.collectionViewTabs]) {
        switch (indexPath.row) {
            case 0:
                size = CGSizeMake(257, 128);
                break;
            case 1:
            case 2:
            case 3:
                size = CGSizeMake(200, 128);
                break;
            default:
                size = CGSizeMake(257, 128);
                break;
        }
    } else {
        size = CGSizeMake(187, 187);
    }
    
    return size;
}

#pragma mark - Action
- (IBAction)btnAddDeviceClick {
    PopoverAction *action1 = [PopoverAction actionWithTitle:@"Add Room" handler:^(PopoverAction *action) {
        UIAlertController *alertController =
        [UIAlertController alertControllerWithTitle:@"Add Room Name"
                                            message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.text = @"";
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [alertController dismissViewControllerAnimated:YES completion:nil];
            UITextField *textField = [[alertController textFields] objectAtIndex:0];
            if ([textField.text isEqualToString:@""]) {
                SCLAlertView *alert = [[SCLAlertView alloc] init];
                [alert showError:self title:@"Error" subTitle:@"You must type room name." closeButtonTitle:@"Ok" duration:0.0f];
                
            } else {
                [alertController dismissViewControllerAnimated:YES completion:nil];
                UITextField *textField = [[alertController textFields] objectAtIndex:0];
                [self.dataRoom addObject:textField.text];
                CTDB._dataRoom = self.dataRoom;
                [self.collectionViewTabs reloadData];
            }
        }];
        UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:otherAction];
        [self presentViewController:alertController animated:TRUE completion:^{
        }];
        
    }];
    
    
    PopoverAction *action2 = [PopoverAction actionWithTitle:@"Add Device" handler:^(PopoverAction *action) {
        ListDeviceViewController *listDevice = [[ListDeviceViewController alloc] initWithNibName:@"ListDeviceViewController" bundle:nil];
        [self.navigationController pushViewController:listDevice animated:YES];
    }];
    PopoverView *popoverView = [PopoverView popoverView];
    popoverView.showShade = YES;
    popoverView.style = PopoverViewStyleDark;
    //popoverView.hideAfterTouchOutside = NO;
    [popoverView showToView:self.view withActions:@[action1, action2]];
    [popoverView showToPoint:CGPointMake(self.btnAddRoomAndDevice.frame.origin.x+60,self.btnAddRoomAndDevice.frame.origin.y+60) withActions:@[action1, action2]];

}

@end
