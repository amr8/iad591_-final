//
//  FanControlViewController.h
//  smarthome
//
//  Created by Andy Nguyen on 6/26/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "FanControlTableViewCell.h"
#import <BFRadialWaveHUD/BFRadialWaveHUD.h>
#import "CTDataCenter.h"
#import "SCLAlertView.h"
#import <AFNetworking/AFNetworking.h>
#import <RMessage/RMessage.h>
#import <RMessage/RMessageView.h>

@import SocketIO;

@interface FanControlViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) SocketManager* manager;
@property (nonatomic,strong) NSMutableArray *listControl;
@property (nonatomic,weak) IBOutlet UITableView *tblListControl;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;
@property (nonatomic,weak) NSString *strTitle;
@property (nonatomic,strong) NSString *deviceName;
@property (nonatomic) BOOL touchAssignButton;
@property (strong, nonatomic) NSString* recievedCode;

@end
