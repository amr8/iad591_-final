//
//  ListDeviceViewController.h
//  smarthome
//
//  Created by Andy Nguyen on 6/26/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListDeviceTableViewCell.h"
#import "CTDataCenter.h"
#import "SCLAlertView.h"
@interface ListDeviceViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
{

}
@property(nonatomic,strong) NSMutableArray *listDeviceName;

- (IBAction)btnBackCLick:(id)sender;

@end
