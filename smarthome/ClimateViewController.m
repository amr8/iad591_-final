//
//  ClimateViewController.m
//  smarthome
//
//  Created by Andy Nguyen on 6/28/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import "ClimateViewController.h"

@interface ClimateViewController ()
{

}
@end

@implementation ClimateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [HumidityView setCircleValue:[self.humidity floatValue] animated:YES];
    [TemperatureView setCircleValue:[self.temperature floatValue] animated:YES];
//    NSURL* url = [[NSURL alloc] initWithString:HOST_URL];
//    self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
//    SocketIOClient* socket = self.manager.defaultSocket;
//    
//    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
//        NSLog(@"socket connected");
//    }];
//    
//    [socket on:@"7e:e9:17:2d:e6:b4_re-temp" callback:^(NSArray* data, SocketAckEmitter* ack) {
//        NSString *cur = [data objectAtIndex:0];
//        NSLog(@"Climate information : %@",cur);
//        NSArray *listClimate = [cur componentsSeparatedByString:@"|"];
//        NSString *temperature = [[[listClimate objectAtIndex:0] componentsSeparatedByString:@" "] objectAtIndex:0];
//        NSString *humidity = [[[listClimate objectAtIndex:1] componentsSeparatedByString:@"%"] objectAtIndex:0];
//        NSLog(@"Temprature : %@",temperature);
//        NSLog(@"Humidity : %@",humidity);
//        NSArray *array = [NSArray arrayWithObjects:temperature,humidity, nil];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_CLIMATE_UI" object:[array mutableCopy]];
//    }];
//    
//    [socket connect];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI:) name:@"UPDATE_CLIMATE_UI" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UPDATE_CLIMATE_UI" object:nil];

}

-(void)updateUI:(id)userInfo {
    [HumidityView setCircleValue:[[[userInfo valueForKey:@"object"] objectAtIndex:1] floatValue] animated:YES];
    [TemperatureView setCircleValue:[[[userInfo valueForKey:@"object"] objectAtIndex:0] floatValue] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Action
- (IBAction)btnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
