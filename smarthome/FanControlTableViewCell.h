//
//  FanControlTableViewCell.h
//  smarthome
//
//  Created by Andy Nguyen on 6/26/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FanControlTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnControl;

@end
