//
//  CTDataCenter.m
//  HMGApp
//
//  Created by Nguyen Duc Ngoc on 7/22/15.
//  Copyright (c) 2015 Mbiz Global. All rights reserved.
//

#import "CTDataCenter.h"

#define CT_DATA_STRING(__KEY__)    \
    - (NSString*) __KEY__\
    {\
        NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@#__KEY__];\
        return value;\
    }\
    - (void)set##__KEY__:(NSString*)aValue\
    {\
        [[NSUserDefaults standardUserDefaults] setObject:aValue forKey:@#__KEY__];\
    }

#define CT_DATA_BOOL(__KEY__)    \
    - (BOOL) __KEY__\
    {\
        return [[NSUserDefaults standardUserDefaults] boolForKey:@#__KEY__];\
    }\
    - (void)set##__KEY__:(BOOL)aValue\
    {\
        [[NSUserDefaults standardUserDefaults] setBool:aValue forKey:@#__KEY__];\
    }

#define CT_DATA_INT(__KEY__)    \
- (NSInteger) __KEY__\
    {\
        return [[NSUserDefaults standardUserDefaults] integerForKey:@#__KEY__];\
    }\
    - (void)set##__KEY__:(NSInteger)aValue\
    {\
        [[NSUserDefaults standardUserDefaults] setInteger:aValue forKey:@#__KEY__];\
    }

#define CT_DATA_OBJECT(__KEY__)    \
- (NSMutableArray*) __KEY__\
{\
return [[NSUserDefaults standardUserDefaults] objectForKey:@#__KEY__];\
}\
- (void)set##__KEY__:(NSMutableArray*)aValue\
{\
[[NSUserDefaults standardUserDefaults] setObject:aValue forKey:@#__KEY__];\
}

#define CT_DATA_DICTIONARY(__KEY__)    \
- (NSMutableDictionary*) __KEY__\
{\
return [[NSUserDefaults standardUserDefaults] objectForKey:@#__KEY__];\
}\
- (void)set##__KEY__:(NSMutableDictionary*)aValue\
{\
[[NSUserDefaults standardUserDefaults] setObject:aValue forKey:@#__KEY__];\
}

@implementation CTDataCenter

CT_DATA_BOOL(_isNotFirstTime);
CT_DATA_INT(_roomIndex);
CT_DATA_STRING(_userName);
CT_DATA_OBJECT(_dataRoom);
//CT_DATA_DICTIONARY(_dataDevicesInRoom);

static CTDataCenter*            s_instance;

+ (CTDataCenter*)inst
{
    if(s_instance == nil)
    {
        @synchronized(self)
        {
            if(s_instance == nil)
            {
                s_instance = [[CTDataCenter alloc] init];
            }
        }
    }
    return s_instance;
}

- (id)init
{
    return self;
}

- (void)reset
{
    self._isNotFirstTime = NO;
    self._userName = nil;
    self._dataRoom = nil;
//    self._dataDevicesInRoom = nil;
    self._roomIndex = 0;
}

@end


