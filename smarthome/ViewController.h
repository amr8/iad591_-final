//
//  ViewController.h
//  smarthome
//
//  Created by Andy Nguyen on 6/25/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "ProgressHUD.h"
#import "ListDeviceViewController.h"
#import "DeviceCollectionViewCell.h"
#import "RoomCollectionViewCell.h"
#import "Constants.h"
#import "CTDataCenter.h"
#import <Popover_OC/PopoverView.h>
#import "ClimateViewController.h"
#import "LightsViewController.h"
#import "TelevisionViewController.h"
#import "ProjectorViewController.h"
#import "AirConditionerViewController.h"
#import "ProjectorViewController.h"
#import "CamerasViewController.h"
#import "SafetyViewController.h"
#import "FanControlViewController.h"

@import SocketIO;

@interface ViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{

}

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblClimate;
@property (weak, nonatomic) IBOutlet UILabel *lblLivingRoomDevices;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTabs;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewDevices;
@property (weak, nonatomic) IBOutlet UIImageView *imgEntranceDoor;
@property (weak, nonatomic) IBOutlet UIButton *btnAddRoomAndDevice;
@property (strong, nonatomic) NSString* temperature;
@property (strong, nonatomic) NSString* humidity;
@property (strong, nonatomic) SocketManager* manager;
@property (strong, nonatomic) NSMutableArray *dataRoom;
@property (strong, nonatomic) NSMutableArray *dataDevice;

- (IBAction)btnAddDeviceClick;

@end

