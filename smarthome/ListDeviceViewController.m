//
//  ListDeviceViewController.m
//  smarthome
//
//  Created by Andy Nguyen on 6/26/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import "ListDeviceViewController.h"

@interface ListDeviceViewController ()

@end

@implementation ListDeviceViewController
@synthesize listDeviceName;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = YES;
    listDeviceName = [NSMutableArray arrayWithObjects: @"Air Conditioner",@"Fan",@"Television",@"Lights",@"Cameras",@"Projector",@"Sound",nil];
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    listDeviceName = [[listDeviceName sortedArrayUsingDescriptors:@[sd]] mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table delegate and datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listDeviceName count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"ListDeviceIdentifier";
    
    ListDeviceTableViewCell *cell = (ListDeviceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell==nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ListDeviceTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.lblDeviceName.text = [listDeviceName objectAtIndex:indexPath.row];

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *temp = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]] mutableCopy];
    if (temp==nil) {
        temp = [[NSMutableArray alloc] init];
    }
    [temp addObject:[listDeviceName objectAtIndex:indexPath.row]];
    [[NSUserDefaults standardUserDefaults] setValue:temp forKey:[NSString stringWithFormat:@"dataDevice_%@_%ld",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];
    NSLog(@"%@ -- %ld",CTDB._dataRoom,CTDB._roomIndex);

    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert showSuccess:self title:@"Congratulations" subTitle:[NSString stringWithFormat:@"Add %@ successful!",[listDeviceName objectAtIndex:indexPath.row]] closeButtonTitle:@"Done" duration:0.0f];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_DEVICE_LIST_UI" object:nil];
}

#pragma mark - Action
- (IBAction)btnBackCLick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
