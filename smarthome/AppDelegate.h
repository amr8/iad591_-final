//
//  AppDelegate.h
//  smarthome
//
//  Created by Andy Nguyen on 6/25/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

