//
//  DeviceCollectionViewCell.h
//  smarthome
//
//  Created by Andy Nguyen on 6/26/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@end
