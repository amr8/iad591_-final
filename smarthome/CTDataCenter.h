//
//  CTDataCenter.h
//  HMGApp
//
//  Created by Nguyen Duc Ngoc on 7/22/15.
//  Copyright (c) 2015 Mbiz Global. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CTDB                [CTDataCenter inst]

@interface CTDataCenter : NSObject{
    
}

+ (CTDataCenter*)inst;

- (void)reset;

@property BOOL    _isNotFirstTime;
@property NSInteger    _roomIndex;
@property (nonatomic, retain) NSString*         _userName;
@property (nonatomic, retain) NSMutableArray*          _dataRoom;
//@property (nonatomic, retain) NSMutableDictionary*     _dataDevicesInRoom;

@end

