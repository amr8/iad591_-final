//
//  FanControlViewController.m
//  smarthome
//
//  Created by Andy Nguyen on 6/26/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import "FanControlViewController.h"

@interface FanControlViewController ()

@end

@implementation FanControlViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld_Device_%@_Control",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex,self.deviceName]];
    [self.lblTitle setText:self.strTitle];
    if (temp==nil) {
//        self.listControl = [NSMutableArray arrayWithObjects: @"Low",@"Middle",@"High",@"Swing",@"ON/OFF",nil];
//        [[NSUserDefaults standardUserDefaults] setValue:self.listControl forKey:[NSString stringWithFormat:@"dataDevice_%@_%ld_Control",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex]];
        self.listControl = [[NSMutableArray alloc] init];
    } else {
        self.listControl = [temp mutableCopy];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table delegate and datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.listControl count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"FanControlIdentifier";
    
    FanControlTableViewCell *cell = (FanControlTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell==nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FanControlTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        [cell.btnControl setTitle:[self.listControl objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        cell.btnControl.tag = indexPath.row;
        [cell.btnControl addTarget:self action:@selector(btnControlClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Action
- (IBAction)btnBackCLick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnAssignCodeClick:(id)sender {
    self.touchAssignButton = YES;
    BFRadialWaveHUD *hud = [[BFRadialWaveHUD alloc] initWithFullScreen:NO
                                                               circles:BFRadialWaveHUD_DefaultNumberOfCircles
                                                           circleColor:nil
                                                                  mode:BFRadialWaveHUDMode_Default
                                                           strokeWidth:BFRadialWaveHUD_DefaultCircleStrokeWidth];
    [hud showInView:self.view];
    
    NSURL* url = [[NSURL alloc] initWithString:HOST_URL];
    self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
    SocketIOClient* socket = self.manager.defaultSocket;
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
    }];
    
    [socket on:@"7e:e9:17:2d:e6:b4_add_data" callback:^(NSArray* data, SocketAckEmitter* ack) {
        if (self.touchAssignButton == NO) {
            return ;
        }
        NSLog(@"Add data : %@",[data objectAtIndex:0]);
        self.recievedCode = [data objectAtIndex:0];
        [hud dismiss];
        
        [RMessage showNotificationWithTitle:@"Congratulations"
                                   subtitle:@"Received code successful! Touch to any button to assign a code."
                                       type:RMessageTypeSuccess
                             customTypeName:nil
                                   duration:2.0
                                   callback:nil];
        [socket disconnect];
        self.touchAssignButton = NO;
//        SCLAlertView *alert = [[SCLAlertView alloc] init];
//        [alert showNotice:@"Congratulations" subTitle:@"Received code successful! Touch to any button to assign a code." closeButtonTitle:@"Ok" duration:0.0f];

    }];
    
    [socket connect];
}

-(IBAction)btnAddControlClick:(id)sender{
    UIAlertController *alertController =
    [UIAlertController alertControllerWithTitle:@"Add Control"
                                        message:nil
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.text = @"";
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [alertController dismissViewControllerAnimated:YES completion:nil];
        UITextField *textField = [[alertController textFields] objectAtIndex:0];

        if ([textField.text isEqualToString:@""]) {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showError:self title:@"Error" subTitle:@"You must type button name." closeButtonTitle:@"Ok" duration:0.0f];

        } else {
            [self.listControl addObject:textField.text];
            NSLog(@"Test Add Control : %@ ",[NSString stringWithFormat:@"dataDevice_%@_%ld_Device_%@_Control",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex,self.deviceName]);
            [[NSUserDefaults standardUserDefaults] setValue:self.listControl forKey:[NSString stringWithFormat:@"dataDevice_%@_%ld_Device_%@_Control",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex,self.deviceName]];
            [self.tblListControl reloadData];
        }
    }];
    UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:otherAction];
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

-(IBAction)btnControlClick:(UIButton *)sender{
    if (self.recievedCode==nil || [self.recievedCode isEqualToString:@""]) {
        NSString *temp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"dataDevice_%@_%ld_Control_%@_%@_%ld",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex,self.deviceName,[self.listControl objectAtIndex:sender.tag],sender.tag]];
        if (temp==nil || [temp isEqualToString:@""]) {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showWarning:self title:@"Warning" subTitle:@"Button has no code!" closeButtonTitle:@"Done" duration:0.0f];
            return;
        }
        
        NSDictionary *params = @{@"key_event": @"send", @"mac_address": @"7e:e9:17:2d:e6:b4",@"push_data":temp};
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:[NSString stringWithFormat:@"%@/api/socket/push",HOST_URL] parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"success: %@",[responseObject description]);
            [RMessage showNotificationWithTitle:@"Congratulations"
                                       subtitle:@"Send code successful!"
                                           type:RMessageTypeSuccess
                                 customTypeName:nil
                                       duration:0.6
                                       callback:nil];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error: %@", error);
            [RMessage showNotificationWithTitle:@"Error"
                                       subtitle:@"Send code fail!"
                                           type:RMessageTypeError
                                 customTypeName:nil
                                       duration:0.6
                                       callback:nil];
        }];
        
    } else {
        [[NSUserDefaults standardUserDefaults] setValue:self.recievedCode forKey:[NSString stringWithFormat:@"dataDevice_%@_%ld_Control_%@_%@_%ld",[CTDB._dataRoom objectAtIndex:CTDB._roomIndex],CTDB._roomIndex,self.deviceName,[self.listControl objectAtIndex:sender.tag],sender.tag]];
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showSuccess:self title:@"Congratulations" subTitle:@"Assign code completed!" closeButtonTitle:@"Done" duration:0.0f];
        self.recievedCode = @"";
    }
}

@end
