//
//  ClimateViewController.h
//  smarthome
//
//  Created by Andy Nguyen on 6/28/18.
//  Copyright © 2018 MSE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "smarthome-Swift.h"

//@import SocketIO;
@interface ClimateViewController : UIViewController
{
    __weak IBOutlet CircularSlider *TemperatureView;
    __weak IBOutlet CircularSlider *HumidityView;
}
//@property (strong, nonatomic) SocketManager* manager;
@property (strong, nonatomic) NSString* temperature;
@property (strong, nonatomic) NSString* humidity;

- (IBAction)btnBackClick:(id)sender;

@end
